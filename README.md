# DC Clusters Simulation

This repo contains the simulation code for the paper 'Passivation of Clustered DC Microgrids with Non-Monotone Loads'.

Running the simulation requires at least Matlab 2022b along with Simulink, Simscape, and the Simscape Electrical Toolbox.

To run the full simulation, load the workspace 'workspace.mat' and ensure that the files 'dc_damping_voltage_follow.ssc', 'dc_damping_voltage_set.ssc', 'dc_zip_load.ssc' and 'option_select.m' are in the same folder as the simulation file. Next, open 'DC_Network.slx' in Simulink and run the simulation.

To run the example of the voltage following bus, ensure that the files 'dc_damping_voltage_follow.ssc', 'dc_damping_voltage_set.ssc', 'dc_zip_load.ssc' and 'option_select.m' are in the same folder as the simulation file. Then, open the 'Example_Voltage_Follow.slx' file in Simulink and run the simulation.

To run the parameter sweep for the Cluster 2 line resistances, ensure all files required for the full simulation are present and run the file 'RunIncreasingResistance.m'.