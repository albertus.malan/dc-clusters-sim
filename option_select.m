classdef option_select < int32
% Enumeration class for specifying whether an option is enabled or disabled

enumeration
    disabled(0)
    enabled(1)
end

methods (Static)
    function map = displayText()
        map = containers.Map;
        map('disabled') = 'Disabled';
        map('enabled') = 'Enabled';
    end
end
end