warning('off','all')
warning

% Setup_DC_Network;
load('workspace.mat');

for i = 1:20
    clear out;

    R_mod([7:10,12:14]) = 5./length([7:10,12:14]) * i;

    out = feval('sim','DC_Network');
    sweepData(i) = out.data_Buses;
%     save(['04_Clustered_Microgrid\01_ParamSweep\R_equal_',num2str(i),'.mat'],'out')
    disp(['-- Complete: R_equal_',num2str(i)])
end

%% Generate Plot
margins = [10 10];

lineColour(1:20) = 5;
lineColour(3:20) = 1;
lineColour(18:20) = 7;

%% Bus Voltages (small scale)
sizeFig1 = [5.8 4];
fig1=figure('Units','centimeters','Position',[margins sizeFig1]);
hold on;

for i = 3:20
    p6(i) = plot(sweepData(i).L6.V,'-','LineWidth',0.8,'SeriesIndex', lineColour(i));
end

for i = 1:2
    p6(i) = plot(sweepData(i).L6.V,'-','LineWidth',1.0,'SeriesIndex', lineColour(i));
end

% p6Base = plot(baseData.L6.V,'k-','LineWidth',1.0);

fig1.PaperUnits = 'centimeters';
fig1.PaperSize = sizeFig1;
fig1.PaperPosition = [0.25 0.25 fig1.PaperSize];

Xlim = [0 0.58];
Ylim = [350 800];

%figure properties
grid;
set(gca,'Fontsize',8);
xlabel('Time (seconds)','Interpreter','latex');
ylabel('Bus voltage (V)','Rotation',90,'Interpreter','latex');
set(gca,'box','on')
set(gca,'Xtick',(0:0.2:1),'Xticklabel',[{'0'},{'0.2'},{'0.4'},{'0.6'},{'0.8'},{'1'}],'Xlim',Xlim)
set(gca,'Ylim',Ylim)

%% Bus Voltages (large scale)
sizeFig2 = [3 4];
fig2=figure('Units','centimeters','Position',[margins sizeFig2]);
hold on;

for i = 3:20
    p6(i) = plot(sweepData(i).L6.V,'-','LineWidth',0.8,'SeriesIndex', lineColour(i));
end

for i = 1:2
    p6(i) = plot(sweepData(i).L6.V,'-','LineWidth',1.0,'SeriesIndex', lineColour(i));
end

% p6Base = plot(baseData.L6.V,'k-','LineWidth',1.0);

fig2.PaperUnits = 'centimeters';
fig2.PaperSize = sizeFig2;
fig2.PaperPosition = [0.25 0.25 fig2.PaperSize];

Xlim = [0.58 0.8];
Ylim = [0 1e4];

%figure properties
grid;
set(gca,'Fontsize',8);
xlabel('Time (seconds)','Interpreter','latex');
set(gca,'box','on')
set(gca,'Xlim',Xlim)
set(gca,'Ylim',Ylim)
set(gca,'YAxisLocation','right')